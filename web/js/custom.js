var gameItem = '<div data-index="{index}" class="col-xs-3 col-md-3 game-item" data-gameid="{id}" id="game-{id}" >' +
    '<a class="thumbnail" href="{url}" title="{name}">' +
    '<img title="{name}" alt="" class="lazy" data-original="{img}">' +
    '<div class=supp>{name}</div>' +
    '</a></div>';


var gamesData = [];

function render(tpl, data) {
    for (var i in data) {
        tpl = tpl.replace(new RegExp('{' + i + '}', 'gi'), data[i]);
    }
    return tpl;
}



var prevTop = 0;

function selectNeighbor(elem) {
    var gamesElem = $('#games div');
    var cName = 'neighbor';
    var cTop = $(elem).position().top;
    if (cTop != prevTop) {
        gamesElem.removeClass(cName);
        console.log('prevTop', prevTop)
    }
    gamesElem.removeClass('active');
    $(elem).addClass('active');
    var top = $(elem).position().top;
    prevTop = top;
    gamesElem.filter(function () {
            return $(this).position().top == top
        }).addClass(cName);

    Game.play(gamesData[$(elem).attr('data-index')], top);
}

function renderTags(tags, elem) {
    var tmp = '';
    for (var i in tags){
        tmp+='<li><a  href="'+tags[i].url+'">'+tags[i].name+'</a></li>';
    }
    $(elem).html(tmp);
}






var Game = {}
Game.show = function (id) {
    var elem = $('#game-'+id).addClass('game').get(0);
    if(elem){
        selectNeighbor(elem);
    }else{
        console.log('Game load')
        API('/api/game/get/'+id,  null, function (game) {
            Page.renderGames(game, true);
            Page.status = 'loading';
            window.onload = function () {
                Game.show(id)
            }
        });
    }
};

Game.close = function () {
    $('#game-window').hide();
    this.hideSWF();
    $('.neighbor').removeClass('game').removeClass('neighbor');
    $(window).trigger('scroll');
}

Game.play = function (gameData, scrollTo) {
    Page.scrollTop(scrollTo);
    Game.showWindow(scrollTo);
    this.hideSWF();
    this.setTitle(gameData.name);
    this.setDescription(gameData.text, gameData.tags);
    window.setTimeout(function () {
        Game.showSWF(gameData, function () {
            $('#game-swf').css('display', 'block');
        })
    },100)

}

Game.restart = function(){
    var html = $('#game-swf').parent().html();
    $('#game-swf').parent().html(html);
}

Game.showWindow = function (pos) {
    $('#game-window').css('top', pos).show();
    $('#game-window .close').on('click', function () {
        Game.close();
    })
}

Game.hideSWF = function(){
    $('#game-swf').hide();
}

Game.showSWF = function (gameData, onload) {
    Page.setTitle(gameData.name);
    Page.setUrl(gameData.url);k

    var el = document.getElementById("game-swf");

    $('#game-swf').show();
    gameData.swf.params.allownetworking = 'internal';

    swfobject.embedSWF(gameData.swf.data, el, '100%', '100%', 10, null, null, gameData.swf.params, null, onload);
}


Game.setDescription = function(text, gameTags){
    var tags = [];
    for (var i in gameTags) {
        tags.push('<a class="tag" onclick="return Page.loadTag(this.href)" href="' + gameTags[i].url + '">' + gameTags[i].name + '</a>');
    }
    $('.game-info .panel-body .description').html(text + '<div class="tags">Tags: ' + tags.join(', ') + '</div>');
}

Game.setTitle = function(title){
    $('#game-window .panel-heading h3').html(title);
}

Game.hide = function () {
    $('#game-window').hide()
}

Game.maxWidth = function (elem) {
    $('#page-container').toggleClass('game-max-width');
    $(elem).toggleClass('glyphicon-resize-full');
    $(elem).toggleClass('glyphicon-resize-small');
}

var Page = {
    status : '',
    nextPage : null
};


Page.renderGames = function (games, prepend) {
    var res = '';
    var ids = {}
    var block_id = '#games';
    for (var i = 0; i < games.length; i++) {
        games[i]['index'] = i+gamesData.length;
        res += render(gameItem, games[i]);
    }
    gamesData = $.merge(gamesData,games);
    if(prepend){
        $(block_id).prepend(res);
    }else{
        $(block_id).append(res);
    }

    for (var i = 0; i < games.length; i++) {
        var gid = '#game-'+games[i].id;
        $(gid + " img.lazy").lazyload();
        $(gid).on('click', function(e){
            var gameId = $(this).data('gameid');
            Game.show(gameId);
            e.preventDefault();
        }).on('mouseover', function () {
            var gameIdx = $(this).data('index');
            $(this).find('.lazy').attr('src', gamesData[gameIdx].img_preview);
        }).on('mouseout',function () {
            var gameIdx = $(this).data('index');
            $(this).find('.lazy').attr('src', gamesData[gameIdx].img);
        })
    }

    return ids;
}

Page.clearGames = function () {
    $('#games').html('');
    Game.hide();
    gamesData = [];
}

Page.setNext = function (nextPage) {
    this.nextPage = nextPage
    console.log(nextPage)
}


Page.goNextPage = function () {
    if(this.nextPage){
        this.loadGames(this.nextPage);
    }
}

Page.loadTag = function (href) {
    $('#game-window').hide();
    this.setUrl(href);
    console.log('load tag');
    this.init();
    return false;
};

Page.scrollTop = function(to){
    $("html, body").stop().animate({scrollTop: to }, '500');
}

Page.init = function() {
    Page.clearGames();
    this.status = 'ready';
    var data = {};
    var parts = window.location.pathname.replace('app_dev.php/','').split('/');
    var path = parts[1];
    var showGame = false;
    if(/[a-z]+[0-9]*/i.exec(path)){
        data.tag = path;
    }else if(matches = /[0-9]+/i.exec(path)){
        showGame = matches[0];
    }

    this.loadGames('/api/main/index', data, function () {
        if(showGame){
            Game.show(showGame);
        }
    });
    $(window).trigger('scroll');
    try{
        (adsbygoogle = window.adsbygoogle || []).push({});
    }catch (e){

    }
};

Page.loadGames = function (url, data, cb) {
    if(this.status != 'ready') return;
    this.status = 'loading';
    API(url, data, function (response) {
        Page.setTitle(response.page.title);

        Page.renderGames(response.games);
        Page.setNext(response.paginator.next);
        Page.status = 'ready';
        if(cb) cb();
        $(window).trigger('scroll');
    });

}

Page.setTitle = function(title){
    $('h1').html(title);
    document.title = title;
}

Page.searchRequest = function (text){
    Page.clearGames();
    API('/api/search', {query: text}, function (response) {
        Page.setTitle(response.page.title);
        if(response.games.length > 0){
            Page.renderGames(response.games);
        }else{
            $('#games').html('<div class="col-xs-6 col-md-6" id="message"><div class="container">'+
                '<div class="alert alert-info" role="alert">'+response.page.message+'</div></div></div> ');
        }

    });
}

Page.search = function(form){
    var value = $(form).find('.search-input').val();
    this.searchRequest(value)
}

Page.setUrl = function (url){
    history.pushState({'statedata':''}, '', url);
    var loc = window.location,
    hashbang = "#!",
    bangIndex = location.href.indexOf(hashbang),
    page = bangIndex != -1 ? loc.href.substring(bangIndex).replace(hashbang, "/") : loc.pathname + loc.search;

    ga('set', 'page', page);
    ga('set', 'title', document.title);
    ga('send', 'pageview');

}

Page.home = function () {
    this.clearGames();
    this.setUrl('/');
    this.init();
    Game.close();

}

function API(url, data, cb, type) {
    type = type ? type : 'GET';
    $.ajax({
        type: type,
        data: data,
        url: 'http://online45.com/' + url,
        dataType: 'json',
        xhrFields: {
            withCredentials: true
        },
        success: cb
    });
}


Page.toggleFull = function () {
    $(window).trigger("Page.fullscreen", true);
    var iframe = document.getElementById('game-swf');
    if (iframe) {
        var elem = iframe;
    } else {
        var elem = document.body; // Make the body go full screen.
    }
    var isInFullScreen = (elem.fullScreenElement && elem.fullScreenElement !== null) || (elem.mozFullScreen || elem.webkitIsFullScreen);

    if (isInFullScreen) {
        Page.cancelFullScreen(document);
    } else {
        Page.requestFullScreen(elem);
    }
    return false;
}

Page.cancelFullScreen = function (el) {
    $(window).trigger("Page.fullscreen", false);
    var requestMethod = el.cancelFullScreen || el.webkitCancelFullScreen || el.mozCancelFullScreen || el.exitFullscreen;
    if (requestMethod) { // cancel full screen.
        requestMethod.call(el);
    } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
        var wscript = new ActiveXObject("WScript.Shell");
        if (wscript !== null) {
            wscript.SendKeys("{F11}");
        }
    }
}

Page.requestFullScreen = function (el) {
    // Supports most browsers and their versions.
    var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen || el.mozRequestFullScreen || el.msRequestFullScreen;

    if (requestMethod) { // Native full screen.
        requestMethod.call(el);
    } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
        var wscript = new ActiveXObject("WScript.Shell");
        if (wscript !== null) {
            wscript.SendKeys("{F11}");
        }
    }
    return false
}




$(document).ready(function () {
    Page.init();
    API('/api/tag/index', null, function (tags) {
        renderTags(tags, '#tag-menu');
        $('#tag-menu a').on('click', function (event) {
            event.preventDefault();
            Page.loadTag(this.href);
        })
    });

});



$(document).ready(function() {
    var win = $(window);

    // Each time the user scrolls
    win.scroll(function() {
        // End of the document reached?
        var end =  $('.loading').position().top - 200;
        if (win.scrollTop() +  win.height() > end) {
            Page.goNextPage();
        }
    });
});