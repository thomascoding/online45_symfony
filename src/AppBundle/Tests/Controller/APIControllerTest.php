<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 28.01.17
 * Time: 11:06
 */

namespace AppBundle\Tests\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class APIControllerTest extends WebTestCase {
  public function testMainIndex(){
    $this->checkAPIUrl('/api/main/index');
  }

  public function testMainTag(){
    $this->checkAPIUrl('/api/main/index?tag=domination');
  }

  public function testMainSearchWord(){
    $this->checkAPIUrl('/api/search?query=war');
  }

  public function testMainSearchWords(){
    $this->checkAPIUrl('/api/search?query=war+war');
  }

  public function checkAPIUrl($url){
    $client = static::createClient();
    $crawler = $client->request('GET', $url);
    $response = $client->getResponse();
    $this->assertEquals(200, $client->getResponse()->getStatusCode());
    $this->checkGames($response->getContent());
  }

  public function checkGames($json){
    $data = json_decode($json, true);
    $this->assertGreaterThan(10, sizeof($data['games']));
    $this->assertNotEmpty($data['page']['title']);
  }

}


