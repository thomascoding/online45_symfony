<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 23.01.17
 * Time: 04:38
 */

namespace AppBundle\Service;


use AppBundle\Entity\Game;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class GameConverterService {
  private $router, $swfPath,$imgPath,$imgPreviewPath;
  function __construct (Router $router, $swfPath, $imgPath, $imgPreviewPath) {
    $this->router = $router;
    $this->swfPath = $swfPath;
    $this->imgPath = $imgPath;
    $this->imgPreviewPath = $imgPreviewPath;
  }

  public function convert(array $data){
    $res = array();

    foreach ($data as $game){
      $tmp = array();
      /**
       * @var $game Game
       */
      $home = 'http://'.$_SERVER['HTTP_HOST'];

      $tmp['id']   = $game->getId();
      $tmp['name'] = $game->getName();
      $tmp['text'] = nl2br($game->getText());
      $tmp['swf'] = array();
      $tmp['swf']['data'] = $game->getSwfData();
      $tmp['tags'] = array();
      foreach ($game->getTags() as $tag){
        $tmp['tags'][] = array(
          'name'=>$tag->getTag(),
          'url'=>$this->router->generate('tag',array('slug'=>$tag->getSlug()))
        );
      }

      if(strpos($game->getSwfData(), 'http')===false) {
        $tmp['swf']['data'] = $this->swfPath.$game->getSwfData();
      }
      $tmp['img'] = $home.str_replace('{id}',$game->getId(),$this->imgPath);
      $tmp['img_preview'] = str_replace('{id}',$game->getId(),$this->imgPreviewPath);
      
      $tmp['swf']['type'] = $game->getSwfType();
      $tmp['url'] = $this->router->generate('game',array('id'=>$game->getId()),true);
      $tmp['swf']['params'] = $this->prepareSWFParams($game->getSwfVars());

      $res[] = $tmp;
    }
    return $res;
  }

  function prepareSWFParams($params){
    $result = array();
    $result['allownetworking'] = 'internal';
    $result['allowScriptAccess'] = 'sameDomain';
    if(preg_match_all('/<param(.+?(\'|"))>/is',$params,$matches)){
      foreach ($matches[1] as $param){
        $name = $value = false;
        if(preg_match('/name=(\'|")([^\'"]+)(\'|")/is',$param,$pName)){
          $name = $pName[2];
        }
        if(preg_match('/value=(\'|")([^\'"]+)(\'|")/is',$param,$pVal)){
          $value = $pVal[2];
        }
        if($name && $value){
          $result[$name] = $value;
        }
      }
    }
    return $result;
  }
}