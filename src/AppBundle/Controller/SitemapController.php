<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 02.02.17
 * Time: 14:47
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Game;
use AppBundle\Entity\GameTags;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class SitemapController extends Controller{
  function indexAction(){
    $res = $this->render('AppBundle:Sitemap:siteindex.xml.twig', array('sitemaps'=>array('tags','games')));
    return $this->response($res->getContent());
  }

  function tagsAction(){
    $tags = $this->getDoctrine()->getRepository('AppBundle:GameTags')->findBy(array('status'=>'published'));
    $urls = array();
    foreach ($tags as $tag){
      /**
       * @var $tag GameTags
       */
      $urls[] = array(
        'loc'=>$this->generateUrl('tag',array('slug'=>$tag->getSlug()), true),
        'changefreq'=>'weekly'
      );
    }
    return $this->response(
      $this->render('AppBundle:Sitemap:sitemap.xml.twig',
        array('urls'=>$urls)
      )->getContent()
    );

  }

  function imagesAction(){
    $service = $this->get('app.service.game_converter_service');
    $games = $this->getDoctrine()->getRepository('AppBundle:Game')->findBy(array('status'=>'published'));
    $urls = $service->convert($games);

    return $this->response(
      $this->render('AppBundle:Sitemap:images.xml.twig',
        array('urls'=>$urls)
      )->getContent()
    );

  }

  function gamesAction(){
    $games = $this->getDoctrine()->getRepository('AppBundle:Game')->findBy(array('status'=>'published'));
    $urls = array();
    foreach ($games as $game){
      /**
       * @var $game Game
       */
      $urls[] = array(
        'loc'=>$this->generateUrl('game',array('id'=>$game->getId()), true),
        'changefreq'=>'weekly',
        'lastmod'=>$game->getDt()->format('c')
      );
    }
    return $this->response(
      $this->render('AppBundle:Sitemap:sitemap.xml.twig',
        array('urls'=>$urls)
      )->getContent()
    );

  }


  private function response ($content,$error=0) {
    $response =  new Response();
    $response->setContent($content);
    $response->setStatusCode(Response::HTTP_OK);
    $response->headers->set('Content-Type', 'text/xml');
    return $response;
  }
}