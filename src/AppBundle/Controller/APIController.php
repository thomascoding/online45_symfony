<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Game;
use AppBundle\Repository\GameRepository;
use AppBundle\Request\GameRequest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Query;

class APIController extends Controller
{

  private function response ($content,$error=0) {
    $response =  new Response();
    $response->setContent($content);
    $response->setStatusCode(Response::HTTP_OK);
    $response->headers->set('Content-Type', 'application/json');
    return $response;
  }

  public function gameAction(Game $game){
    return $this->response(

      json_encode($this->prepareGameData(array($game)),JSON_PRETTY_PRINT)
    );
  }

  public function searchAction(Request $request){
    $gameRequest = new GameRequest();
    $gameRequest->exchangeHttpRequest($request);
    $gameRequest->setIPP($this->getParameter('games.ipp'));
    $gameRepo = $this->getDoctrine()->getRepository('AppBundle:Game');

    $games = $gameRepo->findAllWithTagsByRequest($gameRequest);

    $response = array();
    $response['games'] = $this->prepareGameData($games);
    $response['page'] = array('title'=>$this->get('translator')->trans('search %query%', array('%query%'=>$request->get('query'))));
    if(empty($response['games'])){
      $response['page']['message'] = $this->get('translator')->trans('not found');
    }
    $response['paginator'] = $this->paginate('api.search', $gameRepo,$gameRequest);
    return $this->response(json_encode($response,JSON_PRETTY_PRINT));
  }

  /**
   * @param Request $request
   * @return Response
   */
  public function mainIndexAction(Request $request) {
    // replace this example code with whatever you need
    $gameRequest = new GameRequest();
    $gameRequest->exchangeHttpRequest($request);

    $gameRepo = $this->getDoctrine()->getRepository('AppBundle:Game');

    /**
     * @var $gameRepo GameRepository
     */
    $gameRequest->setIPP($this->getParameter('games.ipp'));

    $data = $gameRepo->findAllWithTagsByRequest($gameRequest);
    //dump($data);
    //dump($this->prepare($data,$this));
    //var_dump($this->getParameter('swf.path'));

    $response = array();
    $response['games'] = $this->prepareGameData($data);
    $response['page'] = array();

    $response['page']['title'] = $this->get('translator')->trans('home');
    $response['paginator'] = $this->paginate('api.main', $gameRepo,$gameRequest);

    if($slug = $gameRequest->getTag()){
      $tag = $this->getDoctrine()->getRepository('AppBundle:GameTags')->findOneBy(array('slug'=>$slug));
      if(!$tag) throw $this->createNotFoundException('Tag not found');
      $response['page']['title'] = $this->get('translator')->trans('tag',array('%tag%'=>$tag->getTag()));
      $response['page']['type'] = 'tag';
    }


    return $this->response(
      json_encode($response,JSON_PRETTY_PRINT)
    );
  }

  function paginate($route, GameRepository $gameRepo, GameRequest $gameRequest){

    $countPages =  ceil($gameRepo->count()/$this->getParameter('games.ipp'));
    $currentPage = $gameRequest->getPage();
    $nextPage = $currentPage + 1;
    $results = array(
      'results'=>$gameRepo->count(),
      'pages'=>$countPages,
      'page'=>$currentPage
    );
    if($nextPage<=$countPages){
      $urlParam = array('page'=>$nextPage);
      if($gameRequest->getTag()){
        $urlParam['tag'] = $gameRequest->getTag();
      }
      if($gameRequest->getSearchQuery()){
        $urlParam['tag'] = $gameRequest->getSearchQuery();
      }

      $results['next'] = $this->generateUrl($route, $urlParam);
    }
    return $results;
  }

  function tagIndexAction(Request $request){
    $repository = $this->getDoctrine()->getRepository('AppBundle:GameTags');

    $results = $repository->findBy(
      array('status' => 'published')
    );

    $tags = array();

    foreach ($results as $tag){
      $tags[] = array(
        'url'=>$this->generateUrl('tag', array('slug'=>$tag->getSlug())),
        'name'=>$tag->getTag()
        );
    }
    return $this->response(json_encode($tags,JSON_PRETTY_PRINT));
  }

  private function prepareGameData($data)
  {
    $service = $this->get('app.service.game_converter_service');
    return $service->convert($data);
  }

}
