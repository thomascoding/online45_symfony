<?php
namespace AppBundle\Request;
use Symfony\Component\HttpFoundation\Request;
class GameRequest
{
  /**
   * @var boolean
   */
  private $published = true;
  /**
   * @var string
   */
  private $name = '';
  private $query = '';
  private $tag = false;
  private $page = 1;
  private $ipp = 0;
  private $resultsCount = 100;
  /**
   * @return int
   */
  public function getPublished()
  {
    return $this->published;
  }
  /**
   * @param int $published
   */
  public function setPublished($published)
  {
    $this->published = (bool)($published);
  }

  /**
   * @param $ipp
   */
  public function setIPP($ipp){
    $this->ipp = $ipp;
  }

  /**
   * @return int
   */
  public function getIPP(){
    return $this->ipp;
  }

  /**
   * @return int
   */
  public function getOffset(){
    return $this->ipp * ($this->getPage() - 1);
  }

  /**
   * @param $query
   */
  public function setSearchQuery($query){
    $this->query = $query;
  }

  /**
   * @param $tag
   */
  public function setTag($tag){
    if(!empty($tag)){
      $this->tag = $tag;
    }
  }

  /**
   * @param $page
   */
  public function setPage($page){
    if($page>1){
      $this->page = $page;
    }
  }

  /**
   * @param $count
   */
  public function setResultsCount($count){
    $this->resultsCount = $count;
  }

  /**
   * @return bool
   */
  public function getTag(){
    return $this->tag;
  }

  /**
   * @return string
   */
  public function getSearchQuery(){
    return $this->query;
  }

  public function getPage(){
    return $this->page;
  }

  /**
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }
  /**
   * @param string $name
   */
  public function setName($name)
  {
    $this->name = strip_tags($name);
  }

  public function exchangeHttpRequest(Request $httpRequest)
  {
    $this->setName($httpRequest->get('name'));
    $this->setTag($httpRequest->get('tag'));
    $this->setSearchQuery($httpRequest->get('query'));
    $this->setPage($httpRequest->get('page'));
  }
}