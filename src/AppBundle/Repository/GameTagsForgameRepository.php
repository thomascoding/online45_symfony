<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 29.01.17
 * Time: 13:03
 */

namespace AppBundle\Repository;


use AppBundle\Entity\GameTags;
use Doctrine\ORM\EntityRepository;

class GameTagsForgameRepository extends EntityRepository{
  public function countGamesByTag(GameTags $tag){
    $tb = $this->createQueryBuilder('gtfg');

    $tb->select($tb->expr()->count('gtfg.idtag'))
     // ->from('AppBundle:GameTagsForgame', 'gtfg')
      ->where('gtfg.idtag = :tag')
      ->setParameter('tag', $tag->getId());

    $query = $tb->getQuery();
    return $query->getSingleScalarResult();
  }
}