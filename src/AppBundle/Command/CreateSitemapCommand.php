<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class CreateSitemapCommand extends Command
{
  protected function configure()
  {
    $this
      // the name of the command (the part after "bin/console")
      ->setName('app:create-sitemap')

      // the short description shown while running "php bin/console list"
      ->setDescription('Creates new sitemap.')

      // the full command description shown when running the command with
      // the "--help" option
      ->setHelp("This command allows you to create sitemap...");
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $output->writeln([
      'Sitemap Creator',
      '============',
      '',
    ]);

  }
}
